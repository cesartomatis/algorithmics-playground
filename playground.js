solution = A => {
  switch (A.length) {
    case 1:
      return -2;
    case 2:
      return checkMin(Math.abs(A[0] - A[1]));
    default:
      return otherCases(A.sort((a, b) => a - b));
  }
};

otherCases = A => {
  min = Math.abs(A[0] - A[1]);
  if (min === 0) {
    return min;
  } else {
    for (i = 1; i < A.length; i++) {
      res = Math.abs(A[i] - A[i + 1]);
      if (res === 0) {
        return res;
      } else if (res < min) {
        min = res;
      }
    }
    return checkMin(min);
  }
};

checkMin = num => {
  if (num > 100000000) {
    return -1;
  } else {
    return num;
  }
};

console.log('L1: ', solution([52]));
console.log('L2: ', solution([22, 3]));
console.log('L2: ', solution([100000000, 200000001]));
console.log('OC: ', solution([0, 3, 3, 7, 5, 3, 11, 1]));
console.log('OC: ', solution([2, 200000000, 300000001]));
